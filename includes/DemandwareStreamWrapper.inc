<?php
/**
 * @file
 * Provides a Demandware dw:// streamwrapper.
 */

/**
 * Demandware stream wrapper class to handle dw:// streams.
 */
class DemandwareStreamWrapper implements DrupalStreamWrapperInterface {
  /**
   * Instance URI (stream).
   *
   * A stream is referenced as "dw://library_id/language/resource".
   *
   * @var String
   */
  protected $uri;

  /**
   * Base implementation of setUri().
   */
  function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * Base implementation of getUri().
   */
  function getUri() {
    return $this->uri;
  }

  /**
   * Returns the local writable target of the resource within the stream.
   *
   * This function should be used in place of calls to realpath() or similar
   * functions when attempting to determine the location of a file. While
   * functions like realpath() may return the location of a read-only file, this
   * method may return a URI or path suitable for writing that is completely
   * separate from the URI used for reading.
   *
   * @param $uri
   *   Optional URI.
   *
   * @return
   *   Returns a string representing a location suitable for writing of a file,
   *   or FALSE if unable to write to the file such as with read-only streams.
   */
  protected function getTarget($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }

    list($scheme, $target) = explode('://', $uri, 2);

    // Remove erroneous leading or trailing, forward-slashes and backslashes.
    return trim($target, '\/');
  }

  /**
   * Returns a web accessible URL for the resource.
   *
   * This function should return a URL that can be embedded in a web page
   * and accessed from a browser. For example, the external URL of
   * "dw://SiteGenesis/default/banners_homepage_05a.jpg" might be
   * "http://example.demandware.net/on/demandware.static/Sites-SiteGenesis-Site/Sites-SiteGenesis-Library/default/v1407764152563/images/homepage/banners_homepage_05a.jpg".
   *
   * @return
   *   Returns a string containing a web accessible URL for the resource.
   */
  public function getExternalUrl(){
    // TODO: Implement function
  }

  /**
   * Returns the MIME type of the resource.
   *
   * @param $uri
   *   The URI, path, or filename.
   * @param $mapping
   *   An optional map of extensions to their mimetypes, in the form:
   *    - 'mimetypes': a list of mimetypes, keyed by an identifier,
   *    - 'extensions': the mapping itself, an associative array in which
   *      the key is the extension and the value is the mimetype identifier.
   *
   * @return
   *   Returns a string containing the MIME type of the resource.
   */
  public static function getMimeType($uri, $mapping = NULL){
    if (!isset($mapping)) {
      // The default file map, defined in file.mimetypes.inc is quite big.
      // We only load it when necessary.
      include_once DRUPAL_ROOT . '/includes/file.mimetypes.inc';
      $mapping = file_mimetype_mapping();
    }

    $extension = '';
    $file_parts = explode('.', drupal_basename($uri));

    // Remove the first part: a full filename should not match an extension.
    array_shift($file_parts);

    // Iterate over the file parts, trying to find a match.
    // For my.awesome.image.jpeg, we try:
    //   - jpeg
    //   - image.jpeg, and
    //   - awesome.image.jpeg
    while ($additional_part = array_pop($file_parts)) {
      $extension = strtolower($additional_part . ($extension ? '.' . $extension : ''));
      if (isset($mapping['extensions'][$extension])) {
        return $mapping['mimetypes'][$mapping['extensions'][$extension]];
      }
    }

    return 'application/octet-stream';
  }

  /**
   * Changes permissions of the resource.
   *
   * No support for file level permissions.
   *
   * @param $mode
   *   Integer value for the permissions. Consult PHP chmod() documentation
   *   for more information.
   *
   * @return
   *   Returns TRUE on success or FALSE on failure.
   */
  public function chmod($mode){
    return TRUE;
  }

  /**
   * Returns canonical, absolute path of the resource.
   *
   * @return bool
   *   Returns FALSE as this wrapper does not provide an implementation.
   */
  public function realpath() {
    return FALSE;
  }

  /**
   * Gets the name of the directory from a given path.
   *
   * This method is usually accessed through drupal_dirname(), which wraps
   * around the normal PHP dirname() function, which does not support stream
   * wrappers.
   *
   * @param string $uri
   *   An optional URI.
   *
   * @return string
   *   A string containing the directory name, or FALSE if not applicable.
   *
   * @see drupal_dirname()
   */
  public function dirname($uri = NULL) {
    list($scheme, $target) = explode('://', $uri, 2);
    $target  = $this->getTarget($uri);
    $dirname = dirname($target);

    if ($dirname === '.') {
      $dirname = '';
    }

    return $scheme . '://' . $dirname;
  }

  public function stream_open($uri, $mode, $options, &$opened_url){
    // TODO: Implement function
  }
  public function stream_close(){
    // TODO: Implement function
  }
  public function stream_lock($operation){
    // TODO: Implement function
  }
  public function stream_read($count){
    // TODO: Implement function
  }
  public function stream_write($data){
    // TODO: Implement function
  }
  public function stream_eof(){
    // TODO: Implement function
  }
  public function stream_seek($offset, $whence){
    // TODO: Implement function
  }
  public function stream_flush(){
    // TODO: Implement function
  }
  public function stream_tell(){
    // TODO: Implement function
  }
  public function stream_stat(){
    // TODO: Implement function
  }
  public function unlink($uri){
    // TODO: Implement function
  }
  public function rename($from_uri, $to_uri){
    // TODO: Implement function
  }
  public function mkdir($uri, $mode, $options){
    // TODO: Implement function
  }
  public function rmdir($uri, $options){
    // TODO: Implement function
  }
  public function url_stat($uri, $flags){
    // TODO: Implement function
  }
  public function dir_opendir($uri, $options){
    // TODO: Implement function
  }
  public function dir_readdir(){
    // TODO: Implement function
  }
  public function dir_rewinddir(){
    // TODO: Implement function
  }
  public function dir_closedir(){
    // TODO: Implement function
  }
}
