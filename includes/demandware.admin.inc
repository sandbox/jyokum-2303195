<?php

/**
 * @file
 * Administrative page callbacks for the Demandware module.
 */

/**
 * Builds and returns the Demandware settings form.
 */
function demandware_admin_settings_form($form, &$form_state) {
  $form['general'] = array(
    '#title' => t('Demandware Instance Settings'),
    '#type' => 'fieldset',
  );
  $form['general']['demandware_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => variable_get('demandware_host', ''),
    '#required' => TRUE,
  );
  $form['general']['demandware_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#default_value' => variable_get('demandware_site_id', ''),
    '#required' => TRUE,
  );
  $form['general']['demandware_instance_type'] = array(
    '#type' => 'select',
    '#title' => t('Instance Type'),
    '#options' => array(
      'sbx' => t('Sandbox'),
      'stg' => t('Development/Staging'),
      'prd' => t('Production'),
    ),
    '#default_value' => variable_get('demandware_instance_type', ''),
    '#required' => TRUE,
  );
  $form['ocapi'] = array(
    '#title' => t('Open Commerce API (OCAPI) Settings'),
    '#type' => 'fieldset',
  );
  $form['ocapi']['demandware_ocapi_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('OCAPI Client ID'),
    '#default_value' => variable_get('demandware_ocapi_client_id', ''),
    '#required' => TRUE,
  );
  $form['ocapi']['demandware_ocapi_client_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('OCAPI Password'),
    '#default_value' => variable_get('demandware_ocapi_client_pass', ''),
    '#required' => TRUE,
  );
  $form['ocapi']['demandware_ocapi_version'] = array(
    '#type' => 'textfield',
    '#title' => t('OCAPI Version'),
    '#default_value' => variable_get('demandware_ocapi_version', ''),
    '#required' => TRUE,
  );
  $form['webdav'] = array(
    '#title' => t('WebDAV Settings'),
    '#type' => 'fieldset',
  );
  $form['webdav']['demandware_webdav_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('demandware_webdav_username', ''),
    '#required' => TRUE,
  );
  $form['webdav']['demandware_webdav_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('demandware_webdav_password', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

function demandware_admin_settings_form_validate($form, &$form_state) {
  $request = array(
    'client_id' => $form_state['values']['demandware_ocapi_client_id'],
    'client_pass' => $form_state['values']['demandware_ocapi_client_pass'],
    'type' => $form_state['values']['demandware_instance_type'],
    'host' => $form_state['values']['demandware_host'],
    'site' => $form_state['values']['demandware_site_id'],
    'version' => $form_state['values']['demandware_ocapi_version'],
  );

  $token = demandware_get_access_token($request['client_id'], $request['client_pass']);
  if ($token) {
    $request['authorization'] = $token->token_type . ' ' . $token->access_token;
    $result = demandware_request('shop', '/site', array(), $request);
    //$result = demandware_request_raw($client_id, $authorization, $host, $site, 'shop', $version, '/site');
    if ($result->code == 200) {
      drupal_set_message('Connection successful', 'status');
    }
    else {
      form_set_error('general', 'Unable to connect to site.');
    }
  }
  else {
    form_set_error('ocapi', 'Unable to obtain an access token.');
  }
}