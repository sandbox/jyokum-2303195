<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('xdebug.var_display_max_data', -1);
ini_set('xdebug.var_display_max_children', -1);
ini_set('xdebug.var_display_max_depth', -1);

/********************************************
 * Test functions
 ********************************************/

function demandware_test() {
//  $result = demandware_customer_search();
//  var_dump($result);
//
//  $result = demandware_meta_shop();
//  var_dump($result);
//  $result = demandware_meta_data();
//  var_dump($result);

//  $result = demandware_libraries_get_folder_structure('SiteGenesis', 'knowledgebase');
//  echo '<pre>';
//  print_r($result);
//  echo '</pre>';
//
//  $result = demandware_libraries_get_folder('SiteGenesis', 'knowledgebase');
//  echo '<pre>';
//  print_r($result);
//  echo '</pre>';

  $result = demandware_account_register('testuser', 'test1234', array('last_name'=>'Test', 'email'=>'e8e864fa4b6902a37bc7@f1a54ce182c498831467dac7f12df0bb.com')); // hopefully a non-deliverable email, DW seems to reject example.com
  var_dump($result);

  $result = demandware_account_login('testuser', 'test1234');
  $cookie = $result->headers['set-cookie'];
  var_dump($result);

  $result = demandware_account('this', $cookie);
  var_dump($result);

//  $result = demandware_product_search('dress');
//  var_dump(json_decode($result->data));
//  demandware_upload_file('/tmp/test.txt');
}

function demandware_libraries_get_folder_structure($library_id, $base_folder_id, $depth = 0) {
  $json = demandware_libraries_get_folder($library_id, $base_folder_id . '/sub_folders');
  $results = json_decode($json->data);
  $folders = array();
  if (isset($results->hits)) {
    foreach ($results->hits as $subfolder) {
      $folders[$subfolder->id] = demandware_libraries_get_folder_structure($library_id, $subfolder->id);
    }
  }
  return $folders;
}

/**
 * Access site information, like site status and site content URLs.
 *
 * @return object
 */
function demandware_site() {
  return demandware_request('shop', '/site');
}

function demandware_meta_shop() {
  $data = array(
    'meta' => 'true',
  );
  $options = array(
    'method' => 'GET',
    'data' => $data,
  );
  return demandware_request('shop', '', $options);
}

function demandware_meta_data() {
  $data = array(
    'meta' => 'true',
  );
  $options = array(
    'method' => 'GET',
    'data' => $data,
  );
  return demandware_request('data', '', $options);
}

function demandware_libraries_get_content($library_id, $content_id) {
  $data = array(
    'q' => $q,
  );
  $options = array(
    'method' => 'GET',
    'data' => $data,
  );
  $env = array(
    'site' => '-',
  );
  $resource = '/libraries/' . $library_id . '/content/' . $content_id;
  return demandware_request('data', $resource, $options,$env);
}

function demandware_product_search($q = NULL) {
  $data = array(
    'q' => $q,
  );
  $options = array(
    'method' => 'GET',
    'data' => $data,
  );
  return demandware_request('shop', '/product_search', $options);
}

function demandware_customer_search() {
  $data = array(
    'query' => "",
  );
  $options = array(
    'method' => 'POST',
    'data' => $data,
  );
  return demandware_request('data', '/customer_search', $options);
}

function demandware_account_login($username, $password) {
  $data = array(
    'username' => $username,
    'password' => $password,
  );
  $options = array(
    'method' => 'POST',
    'data' => $data,
  );
  $resource = "/account/login";
  return demandware_request('shop', $resource, $options);
}

function demandware_account($username, $cookie = "") {
  $data = array(

  );
  $options = array(
    'method' => 'GET',
    'data' => $data,
    'headers' => ['Cookie' => $cookie],
  );
  $resource = "/account/" . $username;
  return demandware_request('shop', $resource, $options);
}

function demandware_account_register($username, $password, $profile = array()) {
  $data = array(
    'credentials' => array(
      'username' => $username,
      'password' => $password,
    ),
    'profile' => $profile,
  );
  $options = array(
    'method' => 'POST',
    'data' => $data,
  );
  return demandware_request('shop', '/account/register', $options);
}

function demandware_options() {
  $options = array(
    'method' => 'OPTIONS',
  );
  return demandware_request('shop', '/content_search', $options);
}